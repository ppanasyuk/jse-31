package ru.t1.panasyuk.tm.exception.entity;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.exception.AbstractException;

@NoArgsConstructor
public abstract class AbstractEntityException extends AbstractException {

    public AbstractEntityException(@NotNull final String message) {
        super(message);
    }

    public AbstractEntityException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityException(@NotNull final Throwable cause) {
        super(cause);
    }

    protected AbstractEntityException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}